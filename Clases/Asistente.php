<?php
require_once "Empleado.php";

class Asistente extends Empleado{
    private $tiempoServicio;
    public function getTiempoServicio():int{
        return $this->tiempoServicio;
    }
    public function setTiempoServicio(int $tiempoServicio):void{
        $this->tiempoServicio=$tiempoServicio;
    }
    public function get_resumen():string{
        $respuesta="<p>Tecnico de ".$this->getJefe()->getArea()."</p>";
        $respuesta.="<p>Experiencia : ".$this->getTiempoServicio()." años</p>";
        return $respuesta;
    }
}
?>