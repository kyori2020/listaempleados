<?php
require_once "Empleado.php";
require_once "JefeArea.php";
class Gerente extends Empleado{
    private $jefes=array();

    public function getCargo():string{
        return "Gerente";
    }
    public function agregarJefeArea(JefeArea $jefe):void{
        array_push($this->jefes,$jefe);
        $jefe->setJefe($this);
    }
    public function get_resumen():string{
        
        return "Gerente";
    }
}
?>