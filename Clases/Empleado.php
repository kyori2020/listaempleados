<?php

class Empleado
{
    private $nombres;
    private $apellidos;
    private $estado="A";
    private $area;
    private $rango;
    private Empleado $jefe;

    public function __construct($nombres,$apellidos) {
        $this->nombres=$nombres;
        $this->apellidos=$apellidos;
    }
    public function setNombres(string $nombres):void {
        $this->nombres=$nombres;
    }
    public function getNombres():string {
        return $this->nombres;
    }
    public function setApellidos(string $apellidos):void {
        $this->apellidos=$apellidos;
    }
    public function getApellidos():string {
        return $this->apellidos;
    }
    public function setdni(string $dni):string {
        $this->dni=$dni;
    }
    public function getDni():string {
        return $this->dni;
    }
    public function setEstado(string $estado):void {
        $this->estado=$estado;
    }
    public function getEstado():string {
        return $this->estado;
    }
    public function getArea():string {
        return $this->area;
    }
    public function setArea(string $area):void {
        $this->area=$area;
    }
    public function getRango():string {
        return $this->rango;
    }
    public function setRango(string $rango):string {
        $this->rango=$rango;
    }
    public function setJefe(Empleado $jefe):void{
        $this->jefe=$jefe;
    }
    public function getJefe():Empleado{
        return $this->jefe;
    }
    public function getNombreCompleto(){
        return $this->apellidos. ' ' . $this->nombres;
    }
    public function get_jefe_inmediato():string{
        if (empty($this->jefe)){
            return '';
        }else{
            return $this->jefe->getNombreCompleto();
        }
        
    }
    public function get_estado():string{
        
        return $this->estado;
    }
}

?>