<?php
require_once 'Asistente.php';
require_once 'Tecnico.php';
class JefeArea extends Empleado{
    private $asistentes=array();
    private $tecnicos=array();
    public function get_resumen():string{
        
        return "Jefe de ". $this->getArea();
    }
    public function agregarAsistente(Asistente $asistente):bool{
        if (count($this->asistentes)<=2){
            array_push($this->asistentes,$asistente);
            $asistente->setJefe($this);
            return true;
        }else{
            return false;
        }
            
       
        
    }
    public function agregarTecnico(Tecnico $tecnico):bool{
        if (count($this->tecnicos)<=5){
            array_push($this->tecnicos,$tecnico);
            $tecnico->setJefe($this);
            return true;
        }else{
            return false;
        }
    }
    public function numeroAsistentes():int{
        return count($this->asistentes);
    }
}


?>