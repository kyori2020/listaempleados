<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdn.tailwindcss.com"></script>
</head>

<body class="bg-slate-100">
<?php
require_once 'Clases/Gerente.php';
require_once 'Clases/JefeArea.php';
require_once 'Clases/Asistente.php';
require_once 'Clases/Tecnico.php';
//Creacion de Gerente
$gerente=new Gerente('Jose Bernardo','Jaramillo Aliaga');
//Creacion de Jefaturas
$jefeLogistica= new JefeArea ('Claudia Marisol','Santos Malpartida');
$jefeLogistica->setArea('Logistica');
$jefeMarketing= new JefeArea ('Maria del Pilar','Torres Flores');
$jefeMarketing->setArea('Marketing');
$jefeSistemas= new JefeArea ('Leonel Andres','Messi Cuccittini');
$jefeSistemas->setArea('Sistemas');
$jefeProduccion= new JefeArea ('Jose Luis','Castillo Terrones');
$jefeProduccion->setArea('Produccion');
//Agregar jefaturas a gerencia
$gerente->agregarJefeArea($jefeMarketing);
$gerente->agregarJefeArea($jefeLogistica);
$gerente->agregarJefeArea($jefeSistemas);
$gerente->agregarJefeArea($jefeProduccion);

// Creacion de asistentes
$asistente1= new Asistente('Joel Mariano','Matos Marin');
$asistente1->setTiempoServicio(4);
$asistente2= new Asistente('Joel Mariano','Matos Marin');
$asistente2->setTiempoServicio(2);

//Agregamos a asistentes a un area
$jefeLogistica->agregarAsistente($asistente1);
$jefeSistemas->agregarAsistente($asistente2);

//Creamos los tecnicos
$tecnico1=new Tecnico("Belinda","Castillo Flores");
$tecnico1->setTiempoServicio(3);
$tecnico2=new Tecnico("Jhon Jairo","Morales Bermudez");
$tecnico2->setTiempoServicio(8);
$tecnico2->setEstado("TC");
//Agregamos a tecnicos a un area
$jefeProduccion->agregarTecnico($tecnico1);
$jefeProduccion->agregarTecnico($tecnico2);
//Lista de trabajadores
$empleados=array();
//Agregamos los trabajadores creados arriba
array_push($empleados,$gerente);
array_push($empleados,$jefeLogistica);
array_push($empleados,$jefeMarketing);
array_push($empleados,$jefeSistemas);
array_push($empleados,$jefeProduccion);
array_push($empleados,$asistente1);
array_push($empleados,$asistente2);
array_push($empleados,$tecnico1);
array_push($empleados,$tecnico2);
?>
<div class="bg-white rounded-lg shadow m-8 p-4" >
    <h1 class=" text-xl text-center ">Lista de Empleados</h1>

    <table class=" w-full mt-6">
        <thead>
            <tr>
                <td class="bg-slate-300 py-4 px-3 font-bold rounded-tl-xl">N°</td>
                <td class="bg-slate-300 py-4 px-3 font-bold">Nombre Completo</td>
                <td class="bg-slate-300 py-4 px-3 font-bold">Resumen</td>
                <td class="bg-slate-300 py-4 px-3 font-bold">Jefe Inmediato</td>
                <td class="bg-slate-300 py-4 px-3 font-bold rounded-tr-xl">Estado</td>
            </tr>
        </thead>
        <tbody>
            <?php
                $i=0;
                foreach($empleados as $emple) {
                $i++;
                $datos='';

                $datos.='<tr class="h-[8px] "></tr>
                <tr class="border">
                    <td class="border-b text-sm px-3 py-2">'.$i;
                    $datos.= '</td>
                    <td class="border-b text-sm px-3 py-2">'.$emple->getNombreCompleto();
                    
                    $datos.='</td>
                    <td class="border-b text-sm px-3 py-2">'.$emple->get_resumen();
                    $datos.='
                    </td>
                    <td class="border-b text-sm px-3 py-2">'.$emple->get_jefe_inmediato();;
                    $datos.='</td>
                    <td class="border-b text-sm px-3 py-2">'.$emple->get_estado();
                $datos.='</td>
                </tr>';
                    echo $datos;
            }
            ?>
        </tbody>
    </table>
</div>

<div class="grid grid-cols-2 w-full	gap-2 ">
    <div class="bg-white mx-8 my-2 rounded-xl p-4 shadow " >
        <h1 class="font-bold">Creacion de objetos</h1>
        <ul class="px-4">
            <li class="list-decimal py-1"><p>Creacion de gerente  <span class=" bg-green-600 text-white rounded-full py-1 px-3 text-xs" >Exito</span></p></li>
            <li class="list-decimal py-1"><p>Creacion de jefes  <span class=" bg-green-600 text-white rounded-full py-1 px-3 text-xs" >Exito</span></p></li>
            <li class="list-decimal py-1"><p>Creacion de tecnico 3 <span class=" bg-red-600 text-white rounded-full py-1 px-3 text-xs" >Error</span></p></li>
        </ul>
    </div>
    <div class=" bg-white mx-8 my-2 rounded-xl p-4 shadow " >
        <h1 class="font-bold text-blue-700" >Integrantes:</h1>
        <ul class="px-4">
            <li class="list-decimal py-1 pl-2"><p> Alan Gabriel Ordoñez Benito </p></li>
            <li class="list-decimal py-1 pl-2"><p> Alexis Jesus Carrasco Delzo </p></li>
            <li class="list-decimal py-1 pl-2"><p> Javier Chacnama Quispe</p></li>
            <li class="list-decimal py-1 pl-2"><p> Joseph George Castro Tovar </p></li>
            <li class="list-decimal py-1 pl-2"><p> Luis Arnulfo Cayo Huachaca </p></li>
        </ul>
    </div>
</div>
</body>
</html>